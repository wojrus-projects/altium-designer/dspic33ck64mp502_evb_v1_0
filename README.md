# Specyfikacja

Urządzenie jest uniwersalną płytką uruchomieniową do MCU Microchip dsPIC33CK64MP502-I/SS.

# Projekt PCB

Schemat: [doc/DSPIC33CK64MP502_EVB_V1_0_SCH.pdf](doc/DSPIC33CK64MP502_EVB_V1_0_SCH.pdf)

Widok 3D: [doc/DSPIC33CK64MP502_EVB_V1_0_3D.pdf](doc/DSPIC33CK64MP502_EVB_V1_0_3D.pdf) (wymaga *Adobe Acrobat Reader DC*)

# Licencja

MIT
